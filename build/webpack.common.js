const path = require('path');
const webpack = require('webpack');

const config = {
    entry: {
        "vendor": path.join(__dirname, '../app/Resources/assets/vendor.js'),
        "login": path.join(__dirname, '../src/AppBundle/Resources/assets/login/login.js'),
        "dashboard": path.join(__dirname, '../src/AppBundle/Resources/assets/dashboard/dashboard.js'),
        "customer_search": path.join(__dirname, '../src/AppBundle/Resources/assets/dashboard/Customer/Search/app.jsx'),
        "test_counter": path.join(__dirname, '../src/AppBundle/Resources/assets/dashboard/Test/Counter/app.jsx'),
        "test_cart": path.join(__dirname, '../src/AppBundle/Resources/assets/dashboard/Test/Cart/app.jsx'),
    },
    output: {
        path: path.join(__dirname, '../web/build/'),
        publicPath: '/',
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: [{
                loader: 'babel-loader',
                options: {presets: [["babel-preset-env", {"modules": false}]]}
            }, {
                loader: 'ify-loader'
            }],

        }, {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
                options: {presets: [["babel-preset-env", {"modules": false}], 'react', 'stage-0']}
            }, {
                loader: 'ify-loader'
            }]

        }, {
            test: /\.css$/,
            use: [
                {loader: "style-loader"},
                {loader: "css-loader"}
            ]
        }, {
            test: /\.(scss)$/,
            use: [{
                loader: 'style-loader', // inject CSS to page
            }, {
                loader: 'css-loader', // translates CSS into CommonJS modules
            }, {
                loader: 'postcss-loader', // Run post css actions
                options: {
                    plugins: function () { // post css plugins, can be exported to postcss.config.js
                        return [
                            require('precss'),
                            require('autoprefixer')
                        ];
                    }
                }
            }, {
                loader: 'sass-loader' // compiles SASS to CSS
            }]
        }, {
            test: /\.html$/,
            use: [{loader: "html-loader"}]
        }, {
            test: /\.(png|jpg)$/,
            use: [{
                loader: 'url-loader',
                options: {limit: 1000}
            }]
        }, {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: "url-loader",
                options: {limit: 1000, mimetype: "application/font-woff", name: "./fonts/[hash].[ext]"}
            }]
        }, {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: "file-loader",
                options: {name: "../build/fonts/[hash].[ext]"}
            }]
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css'],
        modules: ["node_modules"],
        alias: {
            "@": path.resolve(__dirname, '..', 'build'),
            'Router': path.resolve(__dirname, '..', '/app/Resources/assets/router.js')
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            Popper: ['popper.js'],
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
            minChunks: 2
        })
    ]
};

module.exports = config;
import customerApi from '../api/customer'
import * as types from '../constants/ActionTypes'

const loadCustomers = customers => ({
    type: types.INITIALIZE_DEFAULTS,
    customers
});

const dispatchAddCustomer = customer => ({
    "type": types.ADD_NEW_CUSTOMER,
    customer
});

export const getAllCustomers = () => dispatch => {
    customerApi.fetch_customers().then(customers => {
        dispatch(loadCustomers(customers))
    })
};

export const addCustomer = () => dispatch => {
    dispatch(dispatchAddCustomer({
        id: 999,
        first_name: 'a',
        last_name: 'b'
    }));
};

export const filterCustomers = filter => dispatch => {
    dispatch({
        type: types.FILTER_CUSTOMERS,
        filter
    });
};
import {customerReducer, getCustomerById, getTotalCustomers} from './customersReducer'
import testCustomers from '../tests/customers.json';

describe('reducers', () => {
    describe('customerReducer', () => {
        let state;
        describe('Initial Customer State', () => {
            beforeEach(() => {
                state = customerReducer({}, {
                    type: 'INITIALIZE_DEFAULTS',
                    customers: testCustomers
                });
            });

            it('contains the customer from the action', () => {
                expect(getCustomerById(state, 1)).toEqual(testCustomers[0]);
                expect(getCustomerById(state, 666)).toEqual(testCustomers[3]);
            });

            it('should not have customer by id', () => {
                expect(getCustomerById(state, Number.MAX_SAFE_INTEGER)).toEqual(undefined)
            });

        });

        describe('When Added new Customer', () => {
            const new_test_customer = {
                id: 4,
                first_name: 'new_first_name',
                last_name: 'new_last_name'
            };

            beforeEach(() => {
                state = customerReducer(state, {
                    type: 'ADD_NEW_RESULT',
                    customer: new_test_customer
                })
            });

            it('Total Customers Should change by 1', () => {
                expect(getTotalCustomers(state)).toEqual(testCustomers.length + 1);
            });

            it('Should be able to find a new customer', () => {
                expect(getCustomerById(state, new_test_customer.id)).toEqual(new_test_customer);
            });
        });


        describe('Testing Filters', () => {
            beforeEach(() => {
                state = customerReducer({}, {
                    type: 'INITIALIZE_DEFAULTS',
                    customers: testCustomers
                });
            });

            it('All customers should not be hidden', () => {
                console.log(state);
                const displayedCustomers = state.filter(customer => customer.display);

                expect(displayedCustomers.length).toEqual(state.length);
            });


            it('Testing Filter', () => {
                const newState = customerReducer(state, {
                    type: 'FILTER_CUSTOMERS',
                    filter: 'first_name_1'
                });

                expect(newState.filter(customer => customer.display).length).toEqual(1);
            });
        });
    })
});
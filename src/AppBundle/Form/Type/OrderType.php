<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Order;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dispatch_id', TextType::class)
            ->add('description', TextareaType::class, ['required' => false])
            ->add('notes', TextareaType::class, ['required' => false])
            ->add('status', EntityType::class, [
                'class' => Order\Status::class,
                'choice_label' => 'name'
            ])
            ->add('is_completed', CheckboxType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_order_type';
    }
}

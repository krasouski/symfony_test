import './login.scss';

class Login {
    init() {
        $('#login-modal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }
}

$(document).ready(() => {
    new Login().init();
});
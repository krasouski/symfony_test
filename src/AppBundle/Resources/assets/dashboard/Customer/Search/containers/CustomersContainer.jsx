import React from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import CustomerRow from '../components/CustomerRow'
import {getAllCustomers} from '../reducers/customersReducer'
import {Button} from 'reactstrap'
import {addCustomer, filterCustomers} from '../actions'
import SearchRow from "../components/SearchRow"

const CustomerContainer = ({customers, addCustomer, filterCustomers}) => {
    const customerItems = customers.map(customer =>
        <CustomerRow key={customer.id} {...customer}/>
    );
    return (<div>
            <SearchRow onSearchChange={filterCustomers}/>
            {customerItems}
            <Button onClick={() => addCustomer()}>Add New</Button>
        </div>
    )
};

CustomerContainer.propTypes = {
    customers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
    })).isRequired,
    addCustomer: PropTypes.func.isRequired,
    filterCustomers: PropTypes.func.isRequired
};


const mapStateToProps = state => {
    console.log('mapToState');
    return {customers: getAllCustomers(state)};
};

export default connect(mapStateToProps, {addCustomer, filterCustomers})(CustomerContainer)
<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name', TextType::class)
            ->add('last_name', TextType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('apartment_number', TextType::class, ['label' => 'Apt #', 'required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('state', TextType::class, ['required' => false])
            ->add('zipcode', IntegerType::class, ['required' => false]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class
        ]);
    }

}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171111213730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $sqlStr = <<<SQL
INSERT INTO `order_status` (`id`, `name`) VALUES (1, 'New');
INSERT INTO `order_status` (`id`, `name`) VALUES (2, 'Scheduled');
INSERT INTO `order_status` (`id`, `name`) VALUES (3, 'Waiting for Parts');

SQL;

        $this->addSql($sqlStr);


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}

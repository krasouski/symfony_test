import React from 'react'
import PropTypes from 'prop-types'

const CustomerRow = ({first_name, last_name, display}) => {
    let className = (display) ? 'row' : 'row hidden';
    return (
        <div className={className}>
            <div className={'col-md-6'}>{first_name}</div>
            <div className={'col-md-6'}>{last_name}</div>
        </div>
    )
};

CustomerRow.propTypes = {
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired
};

export default CustomerRow;
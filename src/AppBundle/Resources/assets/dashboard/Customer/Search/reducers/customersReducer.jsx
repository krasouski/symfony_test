import {ADD_NEW_CUSTOMER, FILTER_CUSTOMERS, INITIALIZE_DEFAULTS} from '../constants/ActionTypes';


export const customerReducer = (state = [], action) => {
    switch (action.type) {
        case INITIALIZE_DEFAULTS:
            return action.customers.reduce((obj, customer) => {
                customer.display = true;
                    obj.push(customer);
                    return obj;
                }, []
            );
        case ADD_NEW_CUSTOMER:
            if (action.customer) {
                return [...state, action.customer]
            }
        case FILTER_CUSTOMERS:
            return state.reduce((obj, customer) => {
                    if (action.filter == "") {
                        obj.push(Object.assign({}, customer, {display: true}))
                    }
                    else if (customer.first_name.startsWith(action.filter) || customer.last_name.startsWith(action.filter)) {
                        obj.push(Object.assign({}, customer, {display: true}));
                    }
                    else {
                        obj.push(Object.assign({}, customer, {display: false}));
                    }
                    return obj;
                }, []
            );
        default:
            return state;
    }
};

export const getTotalCustomers = (state) => {
    return state.length;
};

export const getAllCustomers = (state) => {
    return state;
};

export const getCustomerById = (state, id) => {
    return state.filter(customer => customer.id == id)[0];
};

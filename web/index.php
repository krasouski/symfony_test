<?php

/**
 * Front controller wrapper around Symfony entry points
 *
 * This allows us to define SYMFONY__ENV in the vhost and choose the correct
 * entry point to jump into.  Notice that there are 2 underscores -- this
 * allows Symfony to pick up and set the variable as well.  It can be found
 * in $_SERVER['env'] or used in a config file as %env%
 */

$env = strtolower(getenv('SYMFONY__ENV'));

if (preg_match('#^dev#', $env)) {
    include_once 'app_dev.php';
} else {
    include_once 'app.php';
}
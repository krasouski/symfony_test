<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Order\Status;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="customer_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="dispatch_id", type="integer", nullable=true)
     */
    private $dispatch_id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_completed", type="boolean")
     */
    private $is_completed = false;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var boolean
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Status")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\Item", mappedBy="order", cascade={"persist", "remove"})
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="orders")
     */
    private $customer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get dispatchId
     *
     * @return int
     */
    public function getDispatchId()
    {
        return $this->dispatch_id;
    }

    /**
     * Set dispatchId
     *
     * @param integer $dispatch_id
     *
     * @return Order
     */
    public function setDispatchId($dispatch_id)
    {
        $this->dispatch_id = $dispatch_id;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Order
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get isComleted
     *
     * @return boolean
     */
    public function getIsCompleted()
    {
        return $this->is_completed;
    }

    /**
     * Set isComleted
     *
     * @param boolean $isComleted
     *
     * @return Order
     */
    public function setIsCompleted($isCompleted)
    {
        $this->is_completed = $isCompleted;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Order\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param \AppBundle\Entity\Order\Status $status
     * @return $this
     */
    public function setStatus(Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add item
     *
     * @param \AppBundle\Entity\Order\Item $item
     *
     * @return Order
     */
    public function addItem(\AppBundle\Entity\Order\Item $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \AppBundle\Entity\Order\Item $item
     */
    public function removeItem(\AppBundle\Entity\Order\Item $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Get customere
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set customere
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return Order
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setCreatedAt(\DateTime $date)
    {
        $this->created_at = $date;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        //$this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}

import React from 'react'
import {FormGroup, Input, Label} from 'reactstrap';

const SearchRow = ({onSearchChange}) => {
    return (
        <div>
            <FormGroup>
                <Label for="search_bar">Search</Label>
                <Input type="text" name="search_bar" placeholder="search here..."
                       onChange={(e) => onSearchChange(e.target.value)}/>
            </FormGroup>
        </div>
    )
};


export default SearchRow;
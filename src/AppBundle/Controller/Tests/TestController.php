<?php

namespace AppBundle\Controller\Tests;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TestController extends Controller
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/counter", name="counter_test")
     */
    public function counterAction(Request $request)
    {
        return $this->render('@App/dashboard/Test/counter.html.twig');
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/cart", name="card_test")
     */
    public function cartAction(Request $request)
    {
        return $this->render('@App/dashboard/Test/cart.html.twig');
    }
}
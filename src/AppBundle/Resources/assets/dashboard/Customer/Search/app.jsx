import React from 'react'
import {render} from 'react-dom'
import {applyMiddleware, createStore} from 'redux'
import {createLogger} from 'redux-logger'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {customerReducer} from './reducers/customersReducer'
import App from './containers/App'
import {getAllCustomers} from './actions'

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger());
}

const store = createStore(
    customerReducer,
    applyMiddleware(...middleware)
);

store.dispatch(getAllCustomers());

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('search-grid-container')
);


<?php

namespace AppBundle\Entity\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemParts
 *
 * @ORM\Table(name="order_item_parts")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Order\ItemPartsRepository")
 */
class ItemParts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=255, nullable=true)
     */
    private $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ordered_date", type="datetime", nullable=true)
     */
    private $orderedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eta_date", type="datetime", nullable=true)
     */
    private $etaDate;

    /**
     * @var string
     *
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     */
    private $receivedDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_installed", type="boolean")
     */
    private $isInstalled = false;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Item", inversedBy="item_parts")
     */
    private $item;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ItemParts
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return ItemParts
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ItemParts
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ItemParts
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get orderedDate
     *
     * @return \DateTime
     */
    public function getOrderedDate()
    {
        return $this->orderedDate;
    }

    /**
     * Set orderedDate
     *
     * @param \DateTime $orderedDate
     *
     * @return ItemParts
     */
    public function setOrderedDate($orderedDate)
    {
        $this->orderedDate = $orderedDate;

        return $this;
    }

    /**
     * Get etaDate
     *
     * @return \DateTime
     */
    public function getEtaDate()
    {
        return $this->etaDate;
    }

    /**
     * Set etaDate
     *
     * @param \DateTime $etaDate
     *
     * @return ItemParts
     */
    public function setEtaDate($etaDate)
    {
        $this->etaDate = $etaDate;

        return $this;
    }

    /**
     * Get recievedDate
     *
     * @return string
     */
    public function getReceivedDate()
    {
        return $this->receivedDate;
    }

    /**
     * Set recievedDate
     *
     * @param string $receivedDate
     *
     * @return ItemParts
     */
    public function setReceivedDate($receivedDate)
    {
        $this->receivedDate = $receivedDate;

        return $this;
    }

    /**
     * Get isInstalled
     *
     * @return bool
     */
    public function getIsInstalled()
    {
        return $this->isInstalled;
    }

    /**
     * Set isInstalled
     *
     * @param boolean $isInstalled
     *
     * @return ItemParts
     */
    public function setIsInstalled($isInstalled)
    {
        $this->isInstalled = $isInstalled;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Order\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Order\Item $item
     *
     * @return ItemParts
     */
    public function setItem(\AppBundle\Entity\Order\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }
}

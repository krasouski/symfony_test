import React from 'react'
import CustomersContainer from './CustomersContainer'

const App = () => (
    <div>
        <CustomersContainer/>
    </div>
);

export default App
import jQuery from 'jquery';

export default {
    fetch_customers: () => jQuery.ajax(Routing.generate('customer_fetch_all'), {method: 'ajax'}).then(data => jQuery.parseJSON(data))
}
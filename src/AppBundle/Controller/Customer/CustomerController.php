<?php

namespace AppBundle\Controller\Customer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Form\Type\CustomerType;
use AppBundle\Form\Type\OrderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CustomerController extends Controller
{

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Customer|null           $customer
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/new", name="customer_new")
     * @Route("/edit/{customer}", name="customer_edit")
     */
    public function newAction(Request $request, Customer $customer = null)
    {
        $em = $this->getDoctrine()->getManager();

        if ($customer === null) {
            $customer = new Customer();
        }

        $customer_orders = $customer->getOrders();

        $customer_form = $this->createForm(CustomerType::class, $customer);

        $order_forms = [];
        foreach ($customer_orders as $custOrder) {
            $order_forms[] = $this->get('form.factory')->createNamed("order_form_{$custOrder->getId()}", OrderType::class, $custOrder);
        }
        //always add new order_form at the end
        $order_forms[] = $this->get('form.factory')->createNamed("order_form_new_form", OrderType::class, new Order());

        $customer_form->handleRequest($request);
        //customer form
        if ($customer_form->isSubmitted() && $customer_form->isValid()) {
            $customer = $customer_form->getData();

            $em->persist($customer);
            $em->flush();

            $this->addFlash('success', 'Customer was successfully saved');
        }

        //loop through order form and check for submitted requests
        foreach ($order_forms as $order_form) {
            $order_form->handleRequest($request);

            //order form
            if ($order_form->isSubmitted() && $order_form->isValid()) {
                $order = $order_form->getData();
                $order->setCustomer($customer);

                $em->persist($order);
                $em->flush();

                $this->addFlash('success', 'Order was successfully saved');
            }
        }

        return $this->render('@App/dashboard/Customer/edit.html.twig',
            [
                'customer_form' => $customer_form->createView(),
                'order_forms' => array_map(function ($order_form) {
                    return $order_form->createView();
                }, $order_forms),
            ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/search", name="customer_search")
     */
    public function searchAction(Request $request)
    {
        return $this->render('@App/dashboard/Customer/search.html.twig', []);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @Route("/fetch_all", name="customer_fetch_all", options={"expose"=true})
     * @Method("AJAX")
     */
    public function fetchAllAction(Request $request)
    {
        $serializer = $this->container->get('jms_serializer');
        $customers = $this->getDoctrine()->getRepository(Customer::class)
            ->findAll();

        return new Response($serializer->serialize($customers, 'json'));
    }
}

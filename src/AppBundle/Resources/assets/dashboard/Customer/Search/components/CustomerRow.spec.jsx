import React from 'react'
import {shallow} from 'enzyme'
import CustomerRow from './CustomerRow'
import TestCustomers from '../tests/customers.json';

const setup = props => {
    const component = shallow(
        <CustomerRow {...props} />
    );

    return {
        component: component
    }
};

describe('Customer Row Component', () => {
    const tCustomer = TestCustomers[0];
    it('should render first name and last name', () => {
        const {component} = setup({'first_name': tCustomer.first_name, 'last_name': tCustomer.last_name});
        expect(component.text()).toMatch(tCustomer.first_name);
        expect(component.text()).toMatch(tCustomer.last_name);
    });
});
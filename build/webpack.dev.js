require('dotenv').config();
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    plugins: [
        new BrowserSyncPlugin({
            proxy: process.env.DEV_URL,
            // Don't open the browser when started
            open: false,
            // Don't mirror clicks across sessions
            ghostMode: false,
            // Let dev server reload
            reload: false,
        })
    ]
});